
# INF214 - Obligatory 2 (deadline: Thursday 3.11.2022)

| Task                        | Points |
|----------------------------|--------|
| Task 1          | __10__      |
| Task 2          | __5__      |
| Task 3          | __10__      |
| Task 4          | __20__      |
| Task 5          | __15__      |
| Total                      | __60__     |
| Required points to pass    | __25__ :tada:     |


## How to submit the assignment?

Submit solutions in a zipped file.

https://mitt.uib.no/courses/36527/assignments/65664

The name of the file should contain your first name, surname and your UiB user handle, e.g., `MikhailBarash_yez013.zip`.


## Task 1 

Using JavaScript's Fetch API, implement a simple application where a user inputs several formulas (e.g.,
"xˆ2 - x*x; 10x-3x") and gets the corresponding simplified expressions (e.g., "0"; "7x").

Use this REST API that performs the simplification of expressions:
https://github.com/aunyks/newton-api/

(Example of a request: https://newton.vercel.app/api/v2/simplify/2x-x)

**You should have a single fetch for every formula, and the requests should not be waiting for
each other.**

**Do not forget to consider the case when fetching fails.**


## Task 2

Modify your solution of Task 1 so that the user is only shown **the first request that gets resolved.**

## Task 3

Implement a function `itmp`, whose behavior is similar to method `.map()` of `Array`,
with the difference that this function `itmp` returns an _iterable_ (rather than an `Array`),
and produces its results on demand.

**The solution should be less than 10 lines of code.**

Here is a skeleton of the solution:

```js
....... itmp(iterable_input, func_to_be_applied) {
  // ..........
}

const p = itmp( [10, 20],  x => x * x );

console.log( p.next() ); // 100
console.log( p.next() ); // 400
```

## Task 4 :bug:

Consider the following code fragment in JavaScript.

```js
function login(req, res) {
    validateLogin(req.nameOrEmail, req.password).then(
        (result) => {
            // (BUG) Why does `result` has value `undefined` here?
        }       
    )
};

function validateLogin(nameOrEmail, password) {
    var errors = {};
    if (validator.isEmpty(nameOrEmail)) {
        errors.nameOrEmail = 'username is required';
    }
    if (validator.isEmpty(password)) {
        errors.password = 'password is required';
    }
    return db.User.find({$or:[{ username: nameOrEmail }, { email: nameOrEmail }]})
        .then(existingUser => {
            if (existingUser.length > 0) {
            // user exists, check if password matches hash
            var user = existingUser[0];
            bcrypt.compare(password, user.password_digest)
                .then(valid => {
                    if (!valid){
                        errors.password = 'Invalid Password';
                    }
                    return { isValid: isEmpty(errors), errors };
                })
            } else {
                errors.nameOrEmail = 'username or email does not exist';
                return { isValid: isEmpty(errors), errors };
            }
        });
}
```

In this code:

- The function `login` calls the function `validateLogin`, passing parameters `nameOrEmail` and `password`.
- The `login` function expects `validateLogin` to return a promise that is eventually resolved with an object that represents whether or not the login was successful.

**A developer has run this code, and on line that starts with the comment `(BUG)`, the `result` parameter is `undefined`. :bug: :bug: :bug: Why is this?**

The developer has the following thoughts about the code.

- Inside `validateLogin`, lines
```js
    var errors = {};
    if (validator.isEmpty(nameOrEmail)) {
        errors.nameOrEmail = 'username is required';
    }
    if (validator.isEmpty(password)) {
        errors.password = 'password is required';
    }
```
perform some simple validation of the values of the parameters `nameOrEmail` and `password` that seems to be unrelated to the problem at hand.
- Line
```js
    return db.User.find({$or:[{ username: nameOrEmail }, ...
```
contains a call `db.User.find(...)` to find a user in the database.
This call returns a promise that is eventually resolved with an object that represents a user with the given `nameOrEmail` and `password`.
- On line
```js
        .then(existingUser => {
```
the programmer calls `then` on this promise to register the function on the lines
```js
            if (existingUser.length > 0) {
            // user exists, check if password matches hash
            var user = existingUser[0];
            bcrypt.compare(password, user.password_digest)
                .then(valid => {
                    if (!valid){
                        errors.password = 'Invalid Password';
                    }
                    return { isValid: isEmpty(errors), errors };
                })
            } else {
                errors.nameOrEmail = 'username or email does not exist';
                return { isValid: isEmpty(errors), errors };
            }
        });
```
as a resolve reaction with this promise.
This creates a dependent promise that is the promise that is actually returned by `validateLogin`.
- At this point, it becomes clear that it is this dependent promise that is eventually resolved with the value `undefined`. Thus, the reason of the bug is somewhere there.

**Help the developer to find the reasons of the bug.**

**You do not need to reimplement the code. Rather, just write a detailed explanation of the reasons of the buggy behaviour.**

## Task 5 :blue_book:

Changes to the EcmaScript programming language (also known under the name of JavaScript) are suggested, discussed, implemented and accepted within the Technical Committee 39 (https://tc39.es/) of the European Computer Manufacturers Association (ECMA International).

Recently, a proposal related to asynchronous programming has been discussed:
https://github.com/tc39/proposal-top-level-await

This proposal became a part of the JavaScript language in June 2021.

**Prepare a presentation summarizing this proposal. You can use the material from the link above, as well as other sources you might find.**

**Try to be creative. Your submission can be, for example, in a form of:**

- **a PowerPoint / Google Slides / PDF presentation (e.g., 2-4 slides);**

_OR_

- **an MP4 video/screen/sound recording (e.g., 2-3 min.);**

_OR_

- **just a detailed text with explanations (e.g., 1 page).**

**You are free to choose the format as you like.**


