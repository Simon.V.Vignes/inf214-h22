# INF214 H22 - Lab 2 - for week 37


## Task 1 (Java)

Define class `Account` with integer field `money`, among other fields/methods that you might need/want.

Implement method
```java
public void send(Account a, Account b, int amount)
```
that allows money transfers between specific accounts.

Make sure that no deadlock can happen.


## Task 2

Assume we have the following shared variables:
```java
int x, y, z;
```

Split the following program into blocks of code that appear atomic:
```java
x = 0;
y = y + 1;
int a = x + y;
a = a * 100;
a = sin(a);
z = a;
```
Is your subdivision unique?


## Task 3

Assume that `x` and `y` are shared variables, and that variable `r` is local.
Assume also that `x == y == 0`.
Consider the following programs:
1. `co x = x + 1; || y = x + 1; oc`
2. `co x = y + 1; || y = x + 1; oc`
3. `co x = y + 1; || x = y + 3; || y = 1; oc`
4. `co x = y + 1; || r = y - 1; || y = 5; oc`

For each of the cases,
* analyze whether each concurrent process has the At-Most-Once-property;
* decide whether the processes are interfering or not;
* and what are the possible resulting values of x and y (assuming sequential consistency).


## Task 4

Consider the following program:
```
co
   <await (x > 0) x = x - 1;>
||
   <await (x < 0) x = x + 2;>
||
   <await (x = 0) x = x - 1;>
oc
```
For which initial values does the program terminate (assuming weakly fair scheduling policy)?

What are the corresponding final values?

Explain your answer.

## Task 5

Consider the following program:
```java
int x = VALUE1;
int y = VALUE2;
x = x + y;
y = x - y;
x = x - y;
```
Add pre- and post-conditions before and after each statement to characterize the effects of this program.

In particular, what are the final values of `x` and `y`?

## Task 6

Consider the following precondition and assignment statement:

```
{x >= 4}    <x = x - 4;>
```

For each of the following triples, show whether the above statement interferes with the triple:

1. `{x >= 0}    <x = x + 5;>    {x >= 5}`
2. `{x >= 0}    <x = x + 5;>    {x >= 0}` 
3. `{x is odd}  <x = x + 5;>    {x is even}`
4. `{x is odd}  <y = x + 1;>    {y is even}`

## Task 7

Consider the following program:
```
int x = 10;
bool c = true;

co
   <await (x==0)>;
   c = false;
||
   while (c) <x = x-1;>
oc
```

* Will the program terminate if scheduling is weakly fair? Explain.
* Will the program terminate if scheduling is strongly fair? Explain.
