# INF214 H22 - Lab 3 - for week 38

_This lab is a small project exercise about barrier synchronization in Java._
_You can access the project's code template here:_
https://replit.com/@mikbar/INF214H22Lab3BarriersJava#Main.java

---

A Toys factory is auditing its doll production in the winter coming.
The process to produce one single doll consists of three different sequential stages:
* part assembly,
* painting process, and
* quality control.

In the third stage, a machine assigns a quality score for a given doll between 0 and 10.
To keep the high reputation of its products, the company has decided to only keep toys with a score higher or equal than 9.

This quality control is the second last stage. The last step consists of packaging dolls for delivering.
We print out details of the toys which pass the process.

**Your task is to help the company with the logistic by implementing a safe concurrent Java solution.**



---

To begin with, study the Java concurrent classes:
```
java.util.concurrent.CyclicBarrier
```
and
```
java.util.concurrent.BrokenBarrierException
```

---

Now, consider the following Java class for the `Doll` objects.
You can add the corresponding getters and setters that are ignored in the following Java definition.

```java
class Doll {
   int id;
   int qualityScoreMachine;
   boolean imperfect, isPainted;
   
   // TODO: implement getters, setters as needed
}
```

---

Complete the missing parts in the following excerpts.
* The method `execution` attempts to produce a given number of dolls.
* There are three stages, _A_, _B_, _C_, representing the corresponding stages in the description above.
To run a new stage, the previous stage must have already finished.
The company considers a stage as "finished" when the complete set of dolls has gone through that stage.
Finally, in the last part of the process we have _D_, the part where the packaging and reporting of good toys takes place.

```java
   private void execution(int dollsNumber) throws InterruptedException {
      stageA = new CyclicBarrier(dollsNumber);
      stageB = new CyclicBarrier(dollsNumber);
      
      // ---------> TODO: stageC = new CyclicBarrier(...); <---------
      
      dolls = new ArrayList<>(dollsNumber);
      for (int i = 0; i < dollsNumber; i++) {
         Process task = new Process(i);
      
         // ---------> TODO: Your solution goes here <---------
      
      }
      try {
         stageC.await();
         System.out.println("Packaging process D");
      
         // ---------> TODO: print results <---------
      
      }
      catch (BrokenBarrierException e) {
         e.printStackTrace();
      }
   }
```

---

Complete the missing part in the `Doll` production task given below:

```java
   class Process implements Runnable {
      int id;
	  
      public Process(int id) {
         this.id = id;
      }
	  
      public void run() {

	         // ---------> TODO: Your solution goes here <---------
     
      }
	  
      void painting(Doll d) {
         d.setPainted(true);
      }
	  
      Doll assembly() {
         Random r = new Random();
         return new Doll(id, r.nextInt(4) + 7);
      }
	  
      void qualityControl(Doll d) {
         if (d.getQualityScore() >= 9) {
            d.hasImperfections(false);
            dolls.add(d);
         }
      }
   }
```


